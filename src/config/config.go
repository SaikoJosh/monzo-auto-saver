package config

import (
	"os"
	"path"
)

var Manager ConfigManager

// Initialise the package.
func init() {
	basePath, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	configPath := path.Join(basePath, "data", "config.json")
	Manager = NewConfigManager(configPath)
}
