package config

import (
	"encoding/json"
	"fmt"
	"os"
	"time"
)

// Represents a pot in the config data file.
type PotConfig struct {
	Key           string  `json:"key"`
	Min           float64 `json:"min"`
	Max           float64 `json:"max"`
	DailyTopUp    float64 `json:"dailyTopUp"`
	MonthlyRefill bool    `json:"monthlyRefill"`
}

// Represents the config data file.
type ConfigData struct {
	Debug                    bool        `json:"debug"`
	DateUpdated              string      `json:"dateUpdated"`
	DateLastRun              string      `json:"dateLastRun"`
	MonzoApiUrl              string      `json:"monzoApiUrl"`
	MonzoRedirectUrl         string      `json:"monzoRedirectUrl"`
	MonzoClientId            string      `json:"monzoClientId"`
	MonzoClientSecret        string      `json:"monzoClientSecret"`
	MonzoAccessToken         string      `json:"monzoAccessToken"`
	MonzoRefreshToken        string      `json:"monzoRefreshToken"`
	MonzoAccountId           string      `json:"monzoAccountId"`
	SlackWebhookUrl          string      `json:"slackWebhookUrl"`
	MinCurrentAccountBalance float64     `json:"minCurrentAccountBalance"`
	EnableMonthlyRefill      bool        `json:"enableMonthlyRefill"`
	Pots                     []PotConfig `json:"pots"`
}

// Represents the config manager class.
type ConfigManager struct {
	FilePath string
	ready    bool
	data     ConfigData
}

// Constructor.
func NewConfigManager(filePath string) ConfigManager {
	cm := ConfigManager{
		FilePath: filePath,
		ready:    false,
	}

	return cm
}

// Reads the config from disk into memory.
func (cm *ConfigManager) LoadConfig() ConfigData {
	fileData, err := os.ReadFile(cm.FilePath)
	if err != nil {
		panic(err)
	}

	json.Unmarshal(fileData, &cm.data)
	cm.ready = true

	return cm.data
}

// Saves the config from memory to disk.
func (cm *ConfigManager) saveConfig() {
	cm.data.DateUpdated = time.Now().Format(time.RFC3339)

	fileData, err := json.MarshalIndent(&cm.data, "", "	")
	if err != nil {
		panic(err)
	}

	err = os.WriteFile(cm.FilePath, fileData, 0644)
	if err != nil {
		panic(err)
	}
}

// Sets a new value.
func (cm *ConfigManager) SetMonzoAccessToken(value string) {
	if !cm.ready {
		err := fmt.Errorf("cannot set config value before initialising")
		panic(err)
	}

	cm.data.MonzoAccessToken = value
	cm.saveConfig()
}

// Sets a new value.
func (cm *ConfigManager) SetMonzoRefreshToken(value string) {
	if !cm.ready {
		err := fmt.Errorf("cannot set config value before initialising")
		panic(err)
	}

	cm.data.MonzoRefreshToken = value
	cm.saveConfig()
}

// Set a new last run date.
func (cm *ConfigManager) SetLastRunDate(value string) {
	if !cm.ready {
		err := fmt.Errorf("cannot set config value before initialising")
		panic(err)
	}

	cm.data.DateLastRun = time.Now().Format(time.RFC3339)
	cm.saveConfig()
}

// Returns the entire config.
func (cm *ConfigManager) Get() ConfigData {
	if !cm.ready {
		err := fmt.Errorf("cannot get config value before initialising")
		panic(err)
	}

	return cm.data
}
