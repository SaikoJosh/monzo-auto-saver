package main

import (
	"fmt"
	"monzo-auto-saver/src/config"
	"monzo-auto-saver/src/log"
	"monzo-auto-saver/src/monzo"
	"monzo-auto-saver/src/saver"
	"monzo-auto-saver/src/slack"
	"monzo-auto-saver/src/utilities"
	"time"
)

// Main entry point.
func main() {
	dryRun := utilities.IsDryRun()

	config.Manager.LoadConfig()
	cfg := config.Manager.Get()
	log.Log.Configure(cfg.Debug, dryRun)

	executionDate := time.Now()
	executionDateStr := executionDate.Format(time.RFC3339)

	log.Log.Info(">> Monzo Auto Saver (" + executionDateStr + ") <<")
	if dryRun {
		log.Log.Info("This is a dry run only, no changes will be made")
	}

	defer func() {
		if err := recover(); err != nil {
			slack.SendSlackMessage(fmt.Sprintf("Monzo Auto Saver ("+executionDateStr+") encountered an error: %s", err))
			log.Log.Info(">> Run Failed! (" + executionDateStr + ") <<")
		}
	}()

	monzo.RefreshMonzoAccessToken()

	lastRunDate := utilities.ParseTime(cfg.DateLastRun)
	if lastRunDate != nil && utilities.SameDay(*lastRunDate, executionDate) && !dryRun {
		log.Log.Info(">> Already Ran Today... Stopping (" + executionDateStr + ") <<")
		return
	}

	accountBalance := monzo.GetAccountBalance()
	accountPots := monzo.GetAccountPots()
	pots := saver.PreparePots(accountPots)
	isMonthStart := utilities.IsMonthStart(executionDate)

	if !cfg.EnableMonthlyRefill {
		log.Log.Info("Monthly refills are disabled")
		isMonthStart = false
	} else if isMonthStart {
		log.Log.Info("It's the start of the month - and monthly refills are enabled!")
	}

	transferList := saver.DeterminePotTransfers(accountBalance, cfg.MinCurrentAccountBalance, pots, isMonthStart)
	monzo.ExecuteTransfers(dryRun, transferList)
	config.Manager.SetLastRunDate(executionDateStr)
	logCache := log.Log.GetCache()
	reportData := saver.GenerateReport(executionDateStr, dryRun, accountBalance, transferList, logCache)
	saver.WriteReport(executionDateStr, reportData)
	slack.SendReportToSlack(executionDateStr, reportData)

	log.Log.Info(">> Run Complete! (" + executionDateStr + ") <<")
}
