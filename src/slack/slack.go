package slack

import (
	"bytes"
	"encoding/json"
	"fmt"
	"monzo-auto-saver/src/config"
	"monzo-auto-saver/src/log"
	"net/http"
	"strconv"
	"time"
)

// Represents the POST body of a Slack webhook request.
type SlackWebhookReqBody struct {
	Text string `json:"text"`
}

// Constants.
var TIMEOUT = time.Duration(30 * time.Second)

// Sends a message to a Slack channel.
func SendSlackMessage(message string) {
	cfg := config.Manager.Get()

	log.Log.Info("Sending message to Slack: %s", cfg.SlackWebhookUrl)

	payload := SlackWebhookReqBody{
		Text: message,
	}
	reqBody, errPayload := json.MarshalIndent(payload, "", "	")
	if errPayload != nil {
		panic(errPayload)
	}

	bodyLen := strconv.Itoa(len(reqBody))
	body := bytes.NewBuffer([]byte(reqBody))
	client := &http.Client{Timeout: TIMEOUT}
	req, errReq := http.NewRequest(http.MethodPost, cfg.SlackWebhookUrl, body)
	if errReq != nil {
		panic(errReq)
	}

	req.Header.Set("accept", "application/json")
	req.Header.Set("cache-control", "no-cache")
	req.Header.Set("content-type", "application/json")
	req.Header.Set("content-length", bodyLen)

	log.Log.HttpRequest(req, string(reqBody))

	res, errDo := client.Do(req)
	if errDo != nil {
		panic(errDo)
	}

	defer res.Body.Close()

	log.Log.Debug("Response status %d received from Slack", res.StatusCode)
	log.Log.HttpResponse(res)

	if res.StatusCode != 200 {
		err := fmt.Errorf("received non-200 status code %d from Slack", res.StatusCode)
		panic(err)
	}

	log.Log.Info("Slack message sent successfully")
}

// Prepares and sends the report to Slack as a message.
func SendReportToSlack(executionDate string, reportData []byte) {
	log.Log.Info("Sending execution report to Slack")
	message := fmt.Sprintf("[%s]\n%s", executionDate, string(reportData))
	SendSlackMessage(message)
}
