package monzo

import (
	"fmt"
	"monzo-auto-saver/src/config"
	"monzo-auto-saver/src/log"
	"monzo-auto-saver/src/utilities"
	"net/url"
	"regexp"
	"strconv"
	"time"
)

// Obtains a new access token from the Monzo API.
func RefreshMonzoAccessToken() *string {
	cfg := config.Manager.Get()
	log.Log.Info("Refreshing access token with Monzo...")

	skipToken := utilities.IsSkipTokenRefresh()
	if skipToken {
		log.Log.Info("Skipping token refresh due to command line arg")
		return nil
	}

	reqBody := url.Values{}
	reqBody.Add("grant_type", "refresh_token")
	reqBody.Add("client_id", cfg.MonzoClientId)
	reqBody.Add("client_secret", cfg.MonzoClientSecret)
	reqBody.Add("refresh_token", cfg.MonzoRefreshToken)
	reqBodyStr := reqBody.Encode()

	resBody := new(ResMonzoRefreshToken)
	makePostRequest(cfg.MonzoAccessToken, cfg.MonzoApiUrl, "/oauth2/token", nil, reqBodyStr, resBody)

	config.Manager.SetMonzoAccessToken(resBody.AccessToken)
	config.Manager.SetMonzoRefreshToken(resBody.RefreshToken)

	return &resBody.AccessToken
}

// Retrieves the main acccount balance from Monzo.
func GetAccountBalance() float64 {
	cfg := config.Manager.Get()
	log.Log.Info("Retrieving account balance from Monzo...")

	qs := url.Values{}
	qs.Add("account_id", cfg.MonzoAccountId)

	resBody := new(ResMonzoAccountBalance)
	makeGetRequest(cfg.MonzoAccessToken, cfg.MonzoApiUrl, "/balance", qs, resBody)

	log.Log.Info("Balance is: £%.2f", float64(resBody.Balance)/100)
	return float64(resBody.Balance)
}

// Retrieves the list of account pots from Monzo.
func GetAccountPots() []MonzoPotDetails {
	cfg := config.Manager.Get()
	log.Log.Info("Retrieving account pots from Monzo...")

	qs := url.Values{}
	qs.Add("current_account_id", cfg.MonzoAccountId)

	resBody := new(ResMonzoAccountPots)
	makeGetRequest(cfg.MonzoAccessToken, cfg.MonzoApiUrl, "/pots", qs, resBody)

	rex, err := regexp.Compile(`(?i)[^a-z0-9#-_\s]+`)
	if err != nil {
		panic(err)
	}

	log.Log.Info(" |--------------------------|-------------|")
	log.Log.Info(" | POT                      | BALANCE     |")
	log.Log.Info(" |--------------------------|-------------|")

	livePots := make([]MonzoPotDetails, len(resBody.Pots))
	count := 0

	for _, potDetails := range resBody.Pots {
		if !potDetails.Deleted {
			nameFormatted := rex.ReplaceAll([]byte(potDetails.Name), []byte(""))
			log.Log.Info(" | %-+24s | £%10.2f |", nameFormatted, float64(potDetails.Balance)/100)
			livePots[count] = potDetails
			count++
		}
	}

	log.Log.Info(" |--------------------------|-------------|")
	log.Log.Info("Found %d live pot(s)", count)

	output := livePots[0:count]
	return output
}

// Triggers Monzo to deposit money from the current account into the given pot.
func DepositIntoPot(dryRun bool, deposit TransferInstruction, index int, count int) {
	cfg := config.Manager.Get()
	log.Log.Info(" > %2d/%2d: Depositing £%.2f to #%s (%s)", index+1, count, float64(deposit.Amount)/100, deposit.PotKey, deposit.PotId)

	if deposit.Direction != EnumTransferDeposit {
		err := fmt.Errorf("transfer instruction (index #%2d) must be a deposit", index)
		panic(err)
	}

	if deposit.Amount <= 0 {
		log.Log.Info(" > %2d/%2d: Skipping nil deposit to #%s", index+1, count, deposit.PotKey)
		return
	}

	date := time.Now().Format("2006-01-02")
	amount := strconv.FormatUint(uint64(deposit.Amount), 10)
	dedupeId := fmt.Sprintf("%s-%s-%s", deposit.PotId, date, amount)

	reqBody := url.Values{}
	reqBody.Add("source_account_id", cfg.MonzoAccountId)
	reqBody.Add("amount", amount)
	reqBody.Add("dedupe_id", dedupeId)
	reqBodyStr := reqBody.Encode()

	resBody := new(ResMonzoPotDeposit)
	path := "/pots/" + deposit.PotId + "/deposit"

	if !dryRun {
		makePutRequest(cfg.MonzoAccessToken, cfg.MonzoApiUrl, path, nil, reqBodyStr, resBody)
	}
}

// Triggers Monzo to withdraw money from the given pot into the current account.
func WithdrawFromPot(dryRun bool, withdrawal TransferInstruction, index int, count int) {
	cfg := config.Manager.Get()
	log.Log.Info(" > %2d/%2d: Withdrawing £%.2f from #%s (%s)", index+1, count, float64(withdrawal.Amount)/100, withdrawal.PotKey, withdrawal.PotId)

	if withdrawal.Direction != EnumTransferWithdrawal {
		err := fmt.Errorf("transfer instruction (index #%2d) must be a withdrawal", index)
		panic(err)
	}

	if withdrawal.Amount <= 0 {
		log.Log.Info(" > %2d/%2d: Skipping nil withdrawal to #%s", index+1, count, withdrawal.PotKey)
		return
	}

	date := time.Now().Format("2006-01-02")
	amount := strconv.FormatUint(uint64(withdrawal.Amount), 10)
	dedupeId := fmt.Sprintf("%s-%s", date, amount)

	reqBody := url.Values{}
	reqBody.Add("destination_account_id", cfg.MonzoAccountId)
	reqBody.Add("amount", amount)
	reqBody.Add("dedupe_id", dedupeId)
	reqBodyStr := reqBody.Encode()

	resBody := new(ResMonzoPotDeposit)
	path := "/pots/" + withdrawal.PotId + "/withdraw"

	if !dryRun {
		makePutRequest(cfg.MonzoAccessToken, cfg.MonzoApiUrl, path, nil, reqBodyStr, resBody)
	}
}

// Executes the given list of transfer instructions against the Monzo API.
func ExecuteTransfers(dryRun bool, transferList []TransferInstruction) {
	log.Log.Info("Executing %d transfer instructions(s)...", len(transferList))

	count := len(transferList)

	for index, transferInstruction := range transferList {
		switch transferInstruction.Direction {
		case EnumTransferDeposit:
			DepositIntoPot(dryRun, transferInstruction, index, count)

		case EnumTransferWithdrawal:
			WithdrawFromPot(dryRun, transferInstruction, index, count)

		default:
			err := fmt.Errorf("transfer instruction direction (index #%2d) is invalid", index)
			panic(err)
		}
	}

	log.Log.Info("Done")
}
