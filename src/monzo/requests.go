package monzo

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"monzo-auto-saver/src/log"
	"monzo-auto-saver/src/utilities"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

// Constants.
var TIMEOUT = time.Duration(30 * time.Second)

// Decodes Monzo's response body into the given struct.
func decodeMonzoResponseBody(res *http.Response, resBody interface{}) {
	log.Log.Info("Response status %d received", res.StatusCode)

	log.Log.HttpResponse(res)

	if res.StatusCode < 200 || res.StatusCode > 299 {
		resErrBody := new(ResMonzoError)
		errJson := utilities.JsonDecode(res.Body, resErrBody)
		if errJson != nil && !errors.Is(errJson, io.EOF) {
			panic(errJson)
		}
		err := fmt.Errorf("received monzo error code %s", resErrBody.Code)
		panic(err)
	} else {
		errJson := utilities.JsonDecode(res.Body, resBody)
		if errJson != nil && !errors.Is(errJson, io.EOF) {
			panic(errJson)
		}
	}
}

// Make a HTTP GET request to Monzo.
func makeGetRequest(accessToken string, host string, path string, qs url.Values, resBody interface{}) {
	uri := host + path
	log.Log.Info("Making HTTP request to GET %s...", uri)

	client := &http.Client{Timeout: TIMEOUT}
	req, errReq := http.NewRequest(http.MethodGet, uri, nil)
	if errReq != nil {
		panic(errReq)
	}

	if qs != nil {
		req.URL.RawQuery = qs.Encode()
	}

	req.Header.Set("accept", "application/json")
	req.Header.Set("cache-control", "no-cache")
	req.Header.Set("authorization", "Bearer "+accessToken)

	log.Log.HttpRequest(req, "")

	res, errDo := client.Do(req)
	if errDo != nil {
		panic(errDo)
	}

	defer res.Body.Close()

	decodeMonzoResponseBody(res, resBody)
}

// Make a HTTP POST request to Monzo.
func makeSubmitRequest(method string, accessToken string, host string, path string, qs url.Values, reqBody string, resBody interface{}) {
	uri := host + path
	log.Log.Info("Making HTTP request to %s %s...", method, uri)

	bodyLen := strconv.Itoa(len(reqBody))
	body := bytes.NewBuffer([]byte(reqBody))
	client := &http.Client{Timeout: TIMEOUT}
	req, errReq := http.NewRequest(method, uri, body)
	if errReq != nil {
		panic(errReq)
	}

	if qs != nil {
		req.URL.RawQuery = qs.Encode()
	}

	req.Header.Set("accept", "application/json")
	req.Header.Set("cache-control", "no-cache")
	req.Header.Set("authorization", "Bearer "+accessToken)
	req.Header.Set("content-type", "application/x-www-form-urlencoded")
	req.Header.Set("content-length", bodyLen)

	log.Log.HttpRequest(req, reqBody)

	res, errDo := client.Do(req)
	if errDo != nil {
		panic(errDo)
	}

	defer res.Body.Close()

	decodeMonzoResponseBody(res, resBody)
}

// Convenience function for making a HTTP POST request to Monzo.
func makePostRequest(accessToken string, host string, path string, qs url.Values, reqBody string, resBody interface{}) {
	makeSubmitRequest(http.MethodPost, accessToken, host, path, qs, reqBody, resBody)
}

// Convenience function for making a HTTP PUT request to Monzo.
func makePutRequest(accessToken string, host string, path string, qs url.Values, reqBody string, resBody interface{}) {
	makeSubmitRequest(http.MethodPut, accessToken, host, path, qs, reqBody, resBody)
}
