package monzo

type ResMonzoError struct {
	Code string `json:"code"`
}

type ReqMonzoRefreshToken struct {
	GrantType    string `json:"grant_type"`
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	RefreshToken string `json:"refresh_token"`
}

type ResMonzoRefreshToken struct {
	AccessToken  string `json:"access_token"`
	ClientId     string `json:"client_id"`
	ExpiresIn    uint64 `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	TokenType    string `json:"token_type"`
	UserId       string `json:"user_id"`
}

type ResMonzoAccountBalance struct {
	Balance      uint64 `json:"balance"`
	TotalBalance uint64 `json:"total_balance"`
	Currency     string `json:"currency"`
}

type MonzoPotDetails struct {
	Id       string `json:"id"`
	Name     string `json:"name"`
	Style    string `json:"style"`
	Balance  uint64 `json:"balance"`
	Currency string `json:"currency"`
	Created  string `json:"created"`
	Updated  string `json:"updated"`
	Deleted  bool   `json:"deleted"`
}

type ResMonzoAccountPots struct {
	Pots []MonzoPotDetails `json:"pots"`
}

type TransferDirection int

const (
	EnumTransferNone TransferDirection = iota
	EnumTransferDeposit
	EnumTransferWithdrawal
)

type TransferInstruction struct {
	Direction TransferDirection `json:"direction"`
	PotId     string            `json:"potId"`
	PotKey    string            `json:"potKey"`
	Amount    float64           `json:"amount"`
}

type ReqMonzoPotDeposit struct {
	source_account_id string
	amount            uint64
	dedupe_id         string
}

type ResMonzoPotDeposit = MonzoPotDetails
