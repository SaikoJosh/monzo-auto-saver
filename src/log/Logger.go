package log

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"strings"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

var PRINT = message.NewPrinter(language.BritishEnglish)
var LOG_CACHE = []string{}

type Logger struct {
	isDebug   bool
	dryRunStr string
}

// Constructor.
func NewLogger() Logger {
	log := Logger{
		isDebug:   false,
		dryRunStr: "",
	}

	return log
}

// Sets the behaviour of the logger.
func (log *Logger) Configure(isDebug bool, isDryRun bool) {
	log.isDebug = isDebug

	if isDryRun {
		log.dryRunStr = "[DRY RUN] "
	}
}

// Returns the log cache array
func (log *Logger) GetCache() []string {
	return LOG_CACHE
}

// Writes log to terminal.
func (log *Logger) write(level string, msg string, args ...interface{}) {
	output := "[" + level + "] " + log.dryRunStr + fmt.Sprintf(msg, args...) + "\n"
	LOG_CACHE = append(LOG_CACHE, output)

	PRINT.Print(output)
}

// Output general log messages.
func (log *Logger) Info(msg string, args ...interface{}) {
	log.write("INFO", msg, args...)
}

// Output debug log messages.
func (log *Logger) Debug(msg string, args ...interface{}) {
	if log.isDebug {
		log.write("DEBUG", msg, args...)
	}
}

// Logs out the given HTTP request.
func (log *Logger) HttpRequest(req *http.Request, reqBody string) {
	dump, err := httputil.DumpRequestOut(req, false)
	if err != nil {
		panic(err)
	}

	if reqBody == "" {
		reqBody = "-no body-"
	}

	dumpStr := strings.TrimSpace(string(dump))
	msg := fmt.Sprintf("Request dump:\n-----\n%v\n-----\n%s\n-----", dumpStr, reqBody)
	log.write("DEBUG", msg)
}

// Logs out the given HTTP response.
func (log *Logger) HttpResponse(res *http.Response) {
	dump, err := httputil.DumpResponse(res, false)
	if err != nil {
		panic(err)
	}

	dumpStr := strings.TrimSpace(string(dump))
	msg := fmt.Sprintf("Response dump:\n-----\n%v\n-----", dumpStr)
	log.write("DEBUG", msg)
}
