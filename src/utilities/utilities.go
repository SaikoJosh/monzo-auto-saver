package utilities

import (
	"encoding/json"
	"io"
	"math"
	"os"
	"reflect"
	"time"
)

// Finds and returns the given command line argument if passed, otherwise returns nil.
func FindCommandLineArg(value string) *string {
	argsWithoutProg := os.Args[1:]

	for _, arg := range argsWithoutProg {
		if arg == value {
			return &arg
		}
	}

	return nil
}

// Returns true if we are on a dry run, otherwise false.
func IsDryRun() bool {
	arg := FindCommandLineArg("--exec")
	return arg == nil
}

// Returns true if we are skipping access token refresh, otherwise false.
func IsSkipTokenRefresh() bool {
	arg := FindCommandLineArg("--skip-token")
	return arg != nil
}

// Decode the given input reader JSON into the given output interface.
func JsonDecode(input io.ReadCloser, output interface{}) error {
	return json.NewDecoder(input).Decode(output)
}

// Returns the given input string as a Time.
func ParseTime(input string) *time.Time {
	if input == "" {
		return nil
	}

	output, err := time.Parse(time.RFC3339, input)
	if err != nil {
		panic(err)
	}

	return &output
}

// Returns true if the given dates are on the same day.
func SameDay(date1 time.Time, date2 time.Time) bool {
	y1, m1, d1 := date1.Date()
	y2, m2, d2 := date2.Date()
	return y1 == y2 && m1 == m2 && d1 == d2
}

// Generic function that returns a typed pointer to the given value.
func Ptr[T any](v T) *T {
	return &v
}

// Generic function that returns the value of the given pointer, or the default value if it is nil.
func PtrValueOrDefault[T any](input *T, defaultValue T) T {
	var output T = defaultValue
	if input != nil {
		output = *input
	}

	return output
}

// Returns the given input if positive, or zero.
func PositiveOrZero(input float64) float64 {
	return math.Max(input, 0)
}

// Returns the value of the given input which is the minimum positive value.
func MinPositiveOrZero(inputs ...float64) float64 {
	var hasSetOnce bool
	var output float64

	for _, input := range inputs {
		if input > 0 && (input < output || !hasSetOnce) {
			hasSetOnce = true
			output = input
		}
	}

	return output
}

// Returns true if the given index is of the last element in the array.
func IsLastArrayElement(arr interface{}, index int) bool {
	len := reflect.ValueOf(arr).Len()
	return index >= len-1
}

// Returns true if the given date is on the first day of the month.
func IsMonthStart(date time.Time) bool {
	return date.Day() == 1
}
