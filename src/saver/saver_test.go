package saver

import (
	"errors"
	"monzo-auto-saver/src/monzo"
	"regexp"
	"testing"
)

type findAccountPotTest struct {
	Name        string
	AccountPots []monzo.MonzoPotDetails
	PotKey      string
	Expected    *monzo.MonzoPotDetails
}

var originalCompileRegexp = compileRegexp

// Utility that converts current amounts in pounds to pence.
func toMoney(amount float64) float64 {
	return 100 * amount
}

// Makes a pot object for testing.
func makePot(key string, min float64, max float64, dailyTopUp float64, monthlyRefill bool, balance float64) *Pot {
	return &Pot{
		Key:           key,
		Min:           toMoney(min),
		Max:           toMoney(max),
		DailyTopUp:    toMoney(dailyTopUp),
		MonthlyRefill: monthlyRefill,
		Balance:       toMoney(balance),
	}
}

// Asserts that the result at the given index meets the given expectations.
func assertInstruction(t *testing.T, result []monzo.TransferInstruction, index int, potKey string, direction monzo.TransferDirection, amount float64) {
	item := result[index]

	if item.PotKey != potKey {
		t.Errorf("bad transfer instruction at index %d - got pot key '#%s' - expected '#%s'", index, item.PotKey, potKey)
	}

	if item.Direction != direction {
		t.Errorf("bad transfer instruction at index %d - got direction '%v' - expected '%v'", index, item.Direction, direction)
	}

	if item.Amount != toMoney(amount) {
		t.Errorf("bad transfer instruction at index %d - got amount '%d' - expected '%d'", index, int(item.Amount/100), int(amount))
	}
}

// Asserts that the result array is the given length.
func assertMaxIndex(t *testing.T, result []monzo.TransferInstruction, maxIndex int) {
	len := len(result)
	qty := maxIndex + 1
	if len != qty {
		t.Errorf("incorrect max index - got '%d' - expected '%d'", len-1, maxIndex)
	}
}

func TestFindAccountPotRegexpCompileError(t *testing.T) {
	compileRegexp = func(s string) (*regexp.Regexp, error) {
		return nil, errors.New("regexp compile error")
	}

	defer func() {
		if err := recover(); err == nil {
			t.Errorf("expected error")
		}
	}()

	accountPots := []monzo.MonzoPotDetails{{Id: "some-id", Name: "Some pot name #KEY"}}
	potKey := "KEY"

	findAccountPot(accountPots, potKey)
}

func TestFindAccountPot(t *testing.T) {
	compileRegexp = originalCompileRegexp

	pot1 := &monzo.MonzoPotDetails{Id: "some-id", Name: "Some pot name #SOME"}
	pot2 := &monzo.MonzoPotDetails{Id: "another-id", Name: "Another pot name #ANO"}
	accountPots := []monzo.MonzoPotDetails{*pot1, *pot2}

	tests := []findAccountPotTest{
		{Name: "pot exists", AccountPots: accountPots, PotKey: "ANO", Expected: pot2},
		{Name: "pot does not exist", AccountPots: accountPots, PotKey: "NOPE", Expected: nil},
	}

	for _, test := range tests {
		t.Run(test.Name, func(t *testing.T) {
			result := findAccountPot(test.AccountPots, test.PotKey)

			if result == nil && test.Expected != nil {
				t.Errorf("got nil, wanted pot %q", test.PotKey)
			}

			if result != nil && test.Expected == nil {
				t.Errorf("got pot %q, wanted nil", test.PotKey)
			}

			if result != nil && test.Expected != nil && result.Id != test.Expected.Id {
				t.Errorf("got pot %q, wanted pot %q", result.Id, test.Expected.Id)
			}
		})
	}
}

func TestAddTransferInstruction(t *testing.T) {
	t.Run("AddDepositInstruction", func(t *testing.T) {
		pot := Pot{Id: "some-id", Key: "KEY"}
		transferInstructions := make([]monzo.TransferInstruction, 0)
		amount := 123.45

		result := addTransferInstruction(&pot, transferInstructions, amount, monzo.EnumTransferDeposit)

		if len(result) != 1 {
			t.Errorf("incorrect array length %d", len(result))
		}

		if result[0].PotId != pot.Id {
			t.Errorf("got %q, wanted %q", result[0].PotId, pot.Id)
		}

		if result[0].PotKey != pot.Key {
			t.Errorf("got %q, wanted %q", result[0].PotKey, pot.Key)
		}

		if result[0].Direction != monzo.EnumTransferDeposit {
			t.Errorf("got %q, wanted %q", result[0].Direction, monzo.EnumTransferDeposit)
		}

		if result[0].Amount != amount {
			t.Errorf("got %f, wanted %f", result[0].Amount, amount)
		}
	})

	t.Run("AddWithdrawalInstruction", func(t *testing.T) {
		pot := Pot{Id: "some-id", Key: "KEY"}
		transferInstructions := make([]monzo.TransferInstruction, 0)
		amount := 123.45

		result := addTransferInstruction(&pot, transferInstructions, amount, monzo.EnumTransferWithdrawal)

		if len(result) != 1 {
			t.Errorf("incorrect array length %d", len(result))
		}

		if result[0].PotId != pot.Id {
			t.Errorf("got %q, wanted %q", result[0].PotId, pot.Id)
		}

		if result[0].PotKey != pot.Key {
			t.Errorf("got %q, wanted %q", result[0].PotKey, pot.Key)
		}

		if result[0].Direction != monzo.EnumTransferWithdrawal {
			t.Errorf("got %q, wanted %q", result[0].Direction, monzo.EnumTransferWithdrawal)
		}

		if result[0].Amount != amount {
			t.Errorf("got %f, wanted %f", result[0].Amount, amount)
		}
	})
}

func TestDeterminePotTransfers(t *testing.T) {
	t.Run("NormalDay", func(t *testing.T) {
		t.Run("FullDailyTopUp", func(t *testing.T) {
			accountBalance := toMoney(1000)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := false

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferDeposit, 15)
			assertMaxIndex(t, result, 0)
		})

		t.Run("PartialDailyTopUp", func(t *testing.T) {
			accountBalance := toMoney(1000)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := false

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 6),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferDeposit, 14)
			assertMaxIndex(t, result, 0)
		})

		t.Run("NoTopUpSpace", func(t *testing.T) {
			accountBalance := toMoney(1000)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := false

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 20),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertMaxIndex(t, result, -1)
		})

		t.Run("OverflowWithdrawal", func(t *testing.T) {
			accountBalance := toMoney(1000)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := false

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 25),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferWithdrawal, 5)
			assertMaxIndex(t, result, 0)
		})

		t.Run("InsufficientCurrentAccountBalance", func(t *testing.T) {
			accountBalance := toMoney(20)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := false

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferDeposit, 10)
			assertMaxIndex(t, result, 0)
		})

		t.Run("RolloverWithSufficientCurrentAccountBalance", func(t *testing.T) {
			accountBalance := toMoney(1000)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := false

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 25),
				makePot("GRO", 100, 100, 5, false, 50),
				makePot("MON", 250, 250, 5, false, 75),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 1, "GRO", monzo.EnumTransferDeposit, 5+20)
			assertInstruction(t, result, 2, "MON", monzo.EnumTransferDeposit, 5)
			assertMaxIndex(t, result, 2)
		})

		t.Run("RolloverWithInsufficientCurrentAccountBalance", func(t *testing.T) {
			accountBalance := toMoney(25)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := false

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 25),
				makePot("GRO", 100, 100, 5, false, 50),
				makePot("MON", 250, 250, 5, false, 75),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 1, "GRO", monzo.EnumTransferDeposit, 5+15)
			assertMaxIndex(t, result, 1)
		})

		t.Run("OverflowToFinalPot", func(t *testing.T) {
			accountBalance := toMoney(1000)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := false

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 25),
				makePot("SAV", 0, 0, 0, false, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 1, "SAV", monzo.EnumTransferDeposit, 20)
			assertMaxIndex(t, result, 1)
		})
	})

	t.Run("MonthStartDay", func(t *testing.T) {
		t.Run("FullRefill", func(t *testing.T) {
			accountBalance := toMoney(15000)
			minCurrentAccountBalance := toMoney(1000)
			isMonthStart := true

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 0),
				makePot("GRO", 100, 100, 5, false, 0),
				makePot("MON", 250, 250, 5, false, 0),
				makePot("BIL", 2500, 2500, 0, true, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferDeposit, 15)
			assertInstruction(t, result, 1, "GRO", monzo.EnumTransferDeposit, 5)
			assertInstruction(t, result, 2, "MON", monzo.EnumTransferDeposit, 5)
			assertInstruction(t, result, 3, "BIL", monzo.EnumTransferDeposit, 2500)
			assertMaxIndex(t, result, 3)
		})

		t.Run("PartialRefill", func(t *testing.T) {
			accountBalance := toMoney(15000)
			minCurrentAccountBalance := toMoney(1000)
			isMonthStart := true

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 0),
				makePot("GRO", 100, 100, 5, false, 0),
				makePot("MON", 250, 250, 5, false, 0),
				makePot("BIL", 2500, 2500, 0, true, 150),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferDeposit, 15)
			assertInstruction(t, result, 1, "GRO", monzo.EnumTransferDeposit, 5)
			assertInstruction(t, result, 2, "MON", monzo.EnumTransferDeposit, 5)
			assertInstruction(t, result, 3, "BIL", monzo.EnumTransferDeposit, 2350)
			assertMaxIndex(t, result, 3)
		})

		t.Run("NoRefillSpace", func(t *testing.T) {
			accountBalance := toMoney(15000)
			minCurrentAccountBalance := toMoney(1000)
			isMonthStart := true

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 0),
				makePot("GRO", 100, 100, 5, false, 0),
				makePot("MON", 250, 250, 5, false, 0),
				makePot("BIL", 2500, 2500, 0, true, 2500),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferDeposit, 15)
			assertInstruction(t, result, 1, "GRO", monzo.EnumTransferDeposit, 5)
			assertInstruction(t, result, 2, "MON", monzo.EnumTransferDeposit, 5)
			assertMaxIndex(t, result, 2)
		})

		t.Run("OverflowWithdrawal", func(t *testing.T) {
			accountBalance := toMoney(15000)
			minCurrentAccountBalance := toMoney(1000)
			isMonthStart := true

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 25),
				makePot("GRO", 100, 100, 5, false, 105),
				makePot("MON", 250, 250, 5, false, 255),
				makePot("BIL", 2500, 2500, 0, true, 0),
				makePot("EMG", 1000, 1000, 0, false, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 1, "GRO", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 2, "MON", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 3, "BIL", monzo.EnumTransferDeposit, 2500)
			assertInstruction(t, result, 4, "EMG", monzo.EnumTransferDeposit, 40)
			assertMaxIndex(t, result, 4)
		})

		t.Run("InsufficientCurrentAccountBalance", func(t *testing.T) {
			accountBalance := toMoney(2000)
			minCurrentAccountBalance := toMoney(1000)
			usableCurrentAccountBalance := (accountBalance - minCurrentAccountBalance) / 100
			isMonthStart := true

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 0),
				makePot("GRO", 100, 100, 5, false, 0),
				makePot("MON", 250, 250, 5, false, 0),
				makePot("BIL", 2500, 2500, 0, true, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferDeposit, 15)
			assertInstruction(t, result, 1, "GRO", monzo.EnumTransferDeposit, 5)
			assertInstruction(t, result, 2, "MON", monzo.EnumTransferDeposit, 5)
			assertInstruction(t, result, 3, "BIL", monzo.EnumTransferDeposit, usableCurrentAccountBalance-15-5-5)
			assertMaxIndex(t, result, 3)
		})

		t.Run("RolloverWithSufficientCurrentAccountBalance", func(t *testing.T) {
			accountBalance := toMoney(15000)
			minCurrentAccountBalance := toMoney(1000)
			isMonthStart := true

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 25),
				makePot("GRO", 100, 100, 5, false, 105),
				makePot("MON", 250, 250, 5, false, 255),
				makePot("BIL", 2500, 2500, 0, true, 1490),
				makePot("EMG", 1000, 1000, 0, false, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 1, "GRO", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 2, "MON", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 3, "BIL", monzo.EnumTransferDeposit, 1010)
			assertInstruction(t, result, 4, "EMG", monzo.EnumTransferDeposit, 40)
			assertMaxIndex(t, result, 4)
		})

		t.Run("RolloverWithInsufficientCurrentAccountBalance", func(t *testing.T) {
			accountBalance := toMoney(2000)
			minCurrentAccountBalance := toMoney(1000)
			usableCurrentAccountBalance := (accountBalance - minCurrentAccountBalance) / 100
			isMonthStart := true

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 25),
				makePot("GRO", 100, 100, 5, false, 105),
				makePot("MON", 250, 250, 5, false, 255),
				makePot("BIL", 2500, 2500, 0, true, 1490),
				makePot("EMG", 1000, 1000, 0, false, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 1, "GRO", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 2, "MON", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 3, "BIL", monzo.EnumTransferDeposit, 1010)
			assertInstruction(t, result, 4, "EMG", monzo.EnumTransferDeposit, usableCurrentAccountBalance+5+5+5-1010)
			assertMaxIndex(t, result, 4)
		})

		t.Run("OverflowToFinalPot", func(t *testing.T) {
			accountBalance := toMoney(1000)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := true

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 25),
				makePot("BIL", 100, 100, 5, true, 25),
				makePot("SAV", 0, 0, 0, false, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 1, "BIL", monzo.EnumTransferDeposit, 75)
			assertInstruction(t, result, 2, "SAV", monzo.EnumTransferDeposit, 25)
			assertMaxIndex(t, result, 2)
		})

		t.Run("RefillFinalPot", func(t *testing.T) {
			accountBalance := toMoney(1000)
			minCurrentAccountBalance := toMoney(10)
			isMonthStart := true

			pots := []*Pot{
				makePot("DAY", 5, 20, 15, false, 25),
				makePot("BIL", 100, 100, 5, true, 25),
				makePot("SAV", 0, 0, 0, true, 0),
			}

			result := DeterminePotTransfers(accountBalance, minCurrentAccountBalance, pots, isMonthStart)

			assertInstruction(t, result, 0, "DAY", monzo.EnumTransferWithdrawal, 5)
			assertInstruction(t, result, 1, "BIL", monzo.EnumTransferDeposit, 75)
			assertInstruction(t, result, 2, "SAV", monzo.EnumTransferDeposit, 25)
			assertMaxIndex(t, result, 2)
		})
	})
}
