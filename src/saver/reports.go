package saver

import (
	"encoding/json"
	"monzo-auto-saver/src/config"
	"monzo-auto-saver/src/log"
	"monzo-auto-saver/src/monzo"
	"os"
	"path"
)

// Represents the report data.
type ReportData struct {
	DateExecuted   string                      `json:"dateExecuted"`
	DryRun         bool                        `json:"dryRun"`
	Debug          bool                        `json:"debug"`
	AccountBalance float64                     `json:"accountBalance"`
	TransferList   []monzo.TransferInstruction `json:"transferList"`
	LogLines       []string                    `json:"logLines"`
}

var reportDir string

// Initialise the package.
func init() {
	basePath, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	reportDir = path.Join(basePath, "data", "reports")
}

// Generates a new execution report and writes it to disk.
func GenerateReport(executionDate string, dryRun bool, accountBalance float64, transferList []monzo.TransferInstruction, logCache []string) []byte {
	log.Log.Info("Generating execution report")

	cfg := config.Manager.Get()
	data := ReportData{
		DateExecuted:   executionDate,
		DryRun:         dryRun,
		Debug:          cfg.Debug,
		AccountBalance: accountBalance,
		TransferList:   transferList,
		LogLines:       logCache,
	}
	reportData, err := json.MarshalIndent(data, "", "	")
	if err != nil {
		panic(err)
	}

	return reportData
}

// Writes the report data to disk.
func WriteReport(executionDate string, reportData []byte) {
	log.Log.Info("Writing execution report to disk")

	filePath := path.Join(reportDir, executionDate+".json")
	err := os.WriteFile(filePath, reportData, 0644)
	if err != nil {
		panic(err)
	}

	log.Log.Info("Report ready: " + filePath)
}
