package saver

import (
	"fmt"
	"math"
	"monzo-auto-saver/src/config"
	"monzo-auto-saver/src/log"
	"monzo-auto-saver/src/monzo"
	"monzo-auto-saver/src/utilities"
	"regexp"
)

// Represents a pot.
type Pot struct {
	Id            string
	Name          string
	Key           string
	Min           float64
	Max           float64
	DailyTopUp    float64
	MonthlyRefill bool
	Balance       float64
	MinTopUpSpace float64
	MaxTopUpSpace float64
}

var compileRegexp = regexp.Compile // Here for testability.

// Finds a pot by its key in the given list of pots.
func findAccountPot(accountPots []monzo.MonzoPotDetails, potKey string) *monzo.MonzoPotDetails {
	rex, err := compileRegexp("(?i)#" + potKey + "(?:\\s|$)")
	if err != nil {
		panic(err)
	}

	for _, accountPot := range accountPots {
		if rex.MatchString(accountPot.Name) {
			return &accountPot
		}
	}

	return nil
}

// Prepares a list of pots by combining the pot config and account pot data for each pot.
func PreparePots(accountPots []monzo.MonzoPotDetails) []*Pot {
	cfg := config.Manager.Get()
	numPots := len(cfg.Pots)
	output := make([]*Pot, numPots)
	count := 0

	log.Log.Info("Preparing %2d pot(s)", numPots)
	log.Log.Info(" |-------|-------------|-------------|-------------|-------------|---------|-------------|-------------|")
	log.Log.Info(" | POT   | MIN         | MAX         | BALANCE     | DAILY       | MONTHLY | MIN SPACE   | MAX SPACE   |")
	log.Log.Info(" |-------|-------------|-------------|-------------|-------------|---------|-------------|-------------|")

	for index, potConfig := range cfg.Pots {
		accountPot := findAccountPot(accountPots, potConfig.Key)
		if accountPot == nil {
			err := fmt.Errorf("cannot find pot matching key '#%s'", potConfig.Key)
			panic(err)
		}

		if potConfig.Min > potConfig.Max {
			err := fmt.Errorf("pot '#%s' config min '%f' is greater than min '%f'", potConfig.Key, potConfig.Min, potConfig.Max)
			panic(err)
		}

		min := potConfig.Min * 100
		max := potConfig.Max * 100
		dailyTopUp := potConfig.DailyTopUp * 100
		currentBalance := float64(accountPot.Balance)
		minTopUpSpace := math.Max(min-currentBalance, 0)
		maxTopUpSpace := math.Max(max-currentBalance, 0)

		log.Log.Info(" | #%-4s | £%10.2f | £%10.2f | £%10.2f | £%10.2f | %7v | £%10.2f | £%10.2f |", potConfig.Key, min/100, max/100, currentBalance/100, dailyTopUp/100, potConfig.MonthlyRefill, minTopUpSpace/100, maxTopUpSpace/100)

		output[index] = &Pot{
			Id:            accountPot.Id,
			Name:          accountPot.Name,
			Key:           potConfig.Key,
			Min:           min,
			Max:           max,
			DailyTopUp:    dailyTopUp,
			MonthlyRefill: potConfig.MonthlyRefill,
			Balance:       currentBalance,
			MinTopUpSpace: minTopUpSpace,
			MaxTopUpSpace: maxTopUpSpace,
		}

		count++
	}

	log.Log.Info(" |-------|-------------|-------------|-------------|-------------|---------|-------------|-------------|")

	return output
}

// Adds the given transfer instruction to the array, unless the direction is 'none' in which case it does nothing.
func addTransferInstruction(pot *Pot, transferInstructions []monzo.TransferInstruction, amount float64, direction monzo.TransferDirection) []monzo.TransferInstruction {
	if direction == monzo.EnumTransferNone {
		return transferInstructions
	}

	instruction := monzo.TransferInstruction{
		Direction: direction,
		PotId:     pot.Id,
		PotKey:    pot.Key,
		Amount:    math.Abs(amount),
	}
	return append(transferInstructions, instruction)
}

// Logs out the given list of transfer instructions in table format.
func logTransferInstructions(transferInstructions []monzo.TransferInstruction) {
	log.Log.Info(" |-------|-------------|-------------------|")
	log.Log.Info(" | POT   | AMOUNT      | DIRECTION         |")
	log.Log.Info(" |-------|-------------|-------------------|")

	var totalTransferAmount float64

	for _, transferInstruction := range transferInstructions {
		potKey := transferInstruction.PotKey
		amount := transferInstruction.Amount
		var direction string

		if transferInstruction.Direction == monzo.EnumTransferDeposit {
			direction = fmt.Sprintf("DEPOSIT (%v)", transferInstruction.Direction)
		} else {
			direction = fmt.Sprintf("WITHDRAWAL (%v)", transferInstruction.Direction)
		}

		totalTransferAmount += amount

		log.Log.Info(" | #%-4s | £%10.2f | %-17s |", potKey, float64(amount)/100, direction)
	}

	log.Log.Info(" |-------|-------------|-------------------|")
	log.Log.Info(" | TOTAL | £%10.2f | (total to move)   |", float64(totalTransferAmount)/100)
	log.Log.Info(" |-------|-------------|-------------------|")
}

// Calculates today's amount, direction, and rollover of the movement of money for the given pot, if any.
func calcPotMovement(pot *Pot, usableAccountBalance *float64, rolloverAmount *float64, isMonthStart bool) (float64, monzo.TransferDirection, float64) {
	potIsUnlimited := pot.Max == 0
	maxTopUpSpace := pot.Max - pot.Balance
	desiredTopUp := pot.DailyTopUp + *rolloverAmount
	var rollover float64

	if isMonthStart && pot.MonthlyRefill {
		desiredTopUp += maxTopUpSpace
		rollover += maxTopUpSpace
	}

	todaysTopUp := math.Min(maxTopUpSpace, desiredTopUp)
	rollover += pot.DailyTopUp - todaysTopUp

	if maxTopUpSpace == 0 && !potIsUnlimited {
		return 0, monzo.EnumTransferNone, rollover
	}

	if maxTopUpSpace < 0 {
		withdrawal := maxTopUpSpace
		return withdrawal, monzo.EnumTransferWithdrawal, rollover
	}

	deposit := math.Min(todaysTopUp, *usableAccountBalance)

	if potIsUnlimited && *rolloverAmount > 0 {
		deposit += *rolloverAmount
	}

	if deposit == 0 {
		return 0, monzo.EnumTransferNone, rollover
	}

	return deposit, monzo.EnumTransferDeposit, rollover
}

// Generates a set of money transfer instructions for moving money from the current account into various pots.
func DeterminePotTransfers(accountBalance float64, minCurrentAccountBalance float64, pots []*Pot, isMonthStart bool) []monzo.TransferInstruction {
	log.Log.Info("Determining amount to transfer to/from each pot...")

	transferInstructions := make([]monzo.TransferInstruction, 0)
	usableAccountBalance := utilities.PositiveOrZero(accountBalance - minCurrentAccountBalance)
	var rolloverAmount float64

	for _, pot := range pots {
		log.Log.Info("Working on pot '%s'", pot.Key)
		amount, direction, rollover := calcPotMovement(pot, &usableAccountBalance, &rolloverAmount, isMonthStart)
		transferInstructions = addTransferInstruction(pot, transferInstructions, amount, direction)
		usableAccountBalance -= amount
		rolloverAmount += rollover
	}

	logTransferInstructions(transferInstructions)

	log.Log.Info("Done")
	return transferInstructions
}
