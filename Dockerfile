# Define global args.
ARG APP_DIR="/opt/monzo-auto-saver"
ARG GO_VERSION="1.20"
ARG ALPINE_VERSION="3.18"

# STAGE 1: BUILD - Include source files, install dev dependencies, and build app.
FROM golang:${GO_VERSION}-alpine${ALPINE_VERSION} AS build-image
ARG APP_DIR
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}
COPY ./go.mod ${APP_DIR}/go.mod
COPY ./go.sum ${APP_DIR}/go.sum
COPY ./src ${APP_DIR}/src
RUN go build -o ./mas ./src/main.go

# STAGE 2: PROD - Copy prod distribution files, and install prod dependencies.
FROM build-image AS final-image
ARG APP_DIR
ENV APP_DIR="${APP_DIR}"
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}
COPY --from=build-image ${APP_DIR}/mas ${APP_DIR}/mas
RUN mkdir -p ${APP_DIR}/data/reports
RUN echo "0 1/6 * * * echo 'Running MAS cron...' && cd ${APP_DIR} && ./mas --exec" >> /etc/crontabs/root

# Run on boot + on schedule.
CMD ["nohup", "sh", "-c", "(cd ${APP_DIR} && ./mas --exec); crond -l 2 -f", "&"]
