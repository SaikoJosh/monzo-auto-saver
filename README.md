# Monzo Auto Saver

Project for auto-saving money from a Monzo current account to several pots on a regular basis.

## Features

- Daily pot top ups.
- Monthly pot refilling.
- Rollovers of any unspent money to subsequent pots.
- Defaults to a dry-run to avoid accidental executions (can be overridden by passing the `--exec` flag).

## Local Development

1. Ensure a config file exists at `./data/config.json`. There is an example file at: `./data/config.example.json`.
2. Add your Monzo API credentials to the config file.
3. Obtain a Monzo access token (see below).
4. Run the app manually: `go run ./src/main.go` (by default the app will perform a dry-run, unless you pass the `--exec` flag).

## Getting a Monzo Access Token

1. Get the Monzo login URL with `./getLoginUrl`.
2. Open the URL in a browser and login to your Monzo account.
3. Click the verification link in the email.
4. After being redirected, copy the oauth code in the callback URL.
5. Approve the access request inside the Monzo mobile app.
6. Get an access token with `./getAccessToken {CODE}`, replacing {CODE} with the oauth code copied earlier.
7. You're now ready to run the app.

## Production Deployment

### Automatic

`WARNING:` Any committed code changes will be pushed and will be deployed immediately. By default, modifications made to `./data/config.json` won't be deployed unless you also pass the `--config` flag.

1. Make sure to push up any changes first.
2. Run: `./deploy --config`.

### Manual

1. First, make sure to push up any code changes you want to deploy, if any.
2. Pull down on the server.
3. Ensure a config file exists at `./data/config.json`. There is an example file at: `./data/config.example.json`.
4. Add your Monzo API credentials to the config file.
5. Obtain a Monzo access token.
6. Start the container: `./start`.

## Useful Scripts

| Script             | Purpose                                                           |
| ------------------ | ----------------------------------------------------------------- |
| `./getLoginUrl`    | Obtains a login URL from Monzo.                                   |
| `./getAccessToken` | Obtains an access token from Monzo and updates the config file.   |
| `./deploy`         | Pushes and deploys code changes and modifications to config file. |
| `./start`          | Starts the Docker container, for production use only.             |
| `./stop`           | Stops the Docker container for production use only.               |

## Command Line Flags

| Script         | Purpose                                                         |
| -------------- | --------------------------------------------------------------- |
| `--exec`       | Forces a production execution, rather than the default dry-run. |
| `--skip-token` | Skips refreshing the Monzo access token.                        |
