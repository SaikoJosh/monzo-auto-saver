package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"monzo-auto-saver/src/config"
	"monzo-auto-saver/src/log"
	"monzo-auto-saver/src/utilities"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

// NOTE: https://docs.monzo.com/#acquire-an-access-token

// Represents the shape of the response body from the Monzo API when obtaining an access token.
type ResMonzoAccessTokenBody struct {
	AccessToken  string `json:"access_token"`
	ClientId     string `json:"client_id"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	TokenType    string `json:"token_type"`
	UserId       string `json:"user_id"`
}

// Constants.
var TIMEOUT = time.Duration(30 * time.Second)

// Obtains and saves in the config an access token from Monzo.
func getAccessToken(code string) {
	config.Manager.LoadConfig()
	cfg := config.Manager.Get()
	log.Log.Configure(true, false)

	reqBody := url.Values{}
	reqBody.Add("grant_type", "authorization_code")
	reqBody.Add("client_id", cfg.MonzoClientId)
	reqBody.Add("client_secret", cfg.MonzoClientSecret)
	reqBody.Add("redirect_uri", cfg.MonzoRedirectUrl)
	reqBody.Add("code", code)
	reqBodyStr := reqBody.Encode()

	bodyLen := strconv.Itoa(len(reqBodyStr))
	body := bytes.NewBuffer([]byte(reqBodyStr))
	client := &http.Client{Timeout: TIMEOUT}
	req, errReq := http.NewRequest(http.MethodPost, "https://api.monzo.com/oauth2/token", body)
	if errReq != nil {
		panic(errReq)
	}

	req.Header.Set("accept", "application/json")
	req.Header.Set("cache-control", "no-cache")
	req.Header.Set("content-type", "application/x-www-form-urlencoded")
	req.Header.Set("content-length", bodyLen)

	log.Log.HttpRequest(req, reqBodyStr)

	res, errDo := client.Do(req)
	if errDo != nil {
		panic(errDo)
	}

	defer res.Body.Close()

	log.Log.Info("Response status %d received", res.StatusCode)
	log.Log.HttpResponse(res)

	if res.StatusCode != 200 {
		err := fmt.Errorf("unexpected status code %d", res.StatusCode)
		panic(err)
	}

	resBody := new(ResMonzoAccessTokenBody)
	errJson := utilities.JsonDecode(res.Body, resBody)
	if errJson != nil && !errors.Is(errJson, io.EOF) {
		panic(errJson)
	}

	config.Manager.SetMonzoAccessToken(resBody.AccessToken)
	config.Manager.SetMonzoRefreshToken(resBody.RefreshToken)

	log.Log.Info("Done!")
}

// Main entry point.
func main() {
	argsWithoutProg := os.Args[1:]
	var code string

	if len(argsWithoutProg) > 0 && argsWithoutProg[0] != "" {
		code = argsWithoutProg[0]
	} else {
		err := fmt.Errorf("code command line argument is required")
		panic(err)
	}

	getAccessToken(code)
}
