package main

import (
	"fmt"
	"monzo-auto-saver/src/config"
	"monzo-auto-saver/src/log"
	"net/url"
)

// NOTE: https://docs.monzo.com/#acquire-an-access-token

// Generates the URL to use to login to Monzo.
func generateLoginUrl() {
	config.Manager.LoadConfig()
	cfg := config.Manager.Get()
	log.Log.Configure(true, false)

	redirectUri := url.QueryEscape(cfg.MonzoRedirectUrl)
	url := fmt.Sprintf("https://auth.monzo.com/?client_id=%s&redirect_uri=%s&response_type=code", cfg.MonzoClientId, redirectUri)

	log.Log.Info("URL: %s", url)
}

// Main entry point.
func main() {
	generateLoginUrl()
}
